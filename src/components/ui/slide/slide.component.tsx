import * as React from 'react';
import {Link} from "react-router-dom";
import './slide.component.css';

const slideStyle = (rows: number, cols?: number, gap?: number) => ({
  height: 'auto',
  width: '100%',
  display: 'grid',
  gridTemplateColumns: `repeat(${cols || 3}, 1fr)`,
  gridTemplateRows: `repeat(${rows}, auto)`,
  gridGap: `${gap}px`,
  marginBottom: rows > 1 ? '5em' : '1em'
});

interface PropsType {
  rows: number;
  cols?: number;
  gap?: number;
  title?: {
    text: string;
    position: 'top' | 'bottom';
  };
  link?: {
    url?: any;
    text: string;
  }
}

class Component extends React.Component<PropsType> {
  render() {
    const {
      rows,
      cols,
      children,
      title,
      link,
      gap
    } = this.props;
    return (
      <div className="slide" style={slideStyle(rows, cols, gap)}>
        {children}
        {
          title &&
            <div className={`title-${title.position}`}>
              <span className="text-big">{title.text}</span>
              <br />
              {
                link &&
                  <span className="text-small">
                    {
                      link.url ?
                        <Link to={link.url}>
                          <span className="linkName">{link.text}</span> <span className="linkArrow">&gt;</span>
                        </Link> : <span className="linkName">{link.text}</span>
                    }
                  </span>
              }
            </div>
        }
      </div>
    );
  }
}

export const Slide = Component;
