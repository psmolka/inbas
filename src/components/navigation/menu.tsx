import * as React from 'react';
import './menu.css';
import {Link} from "react-router-dom";

const Component: React.FC = () => {
  return (
    <nav>
      <div className="links">
        <Link to="/"><span className="home">INSTALBAS</span></Link>
        <div className="spacer"></div>
        <Link to="/dsk/">Datenschutzerklärung</Link>
        <Link to="/contact/">Kontakt</Link>
      </div>
    </nav>
  );
};

export const Menu = Component;
