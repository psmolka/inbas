import * as React from 'react';
import './home.component.css';
import {Sticker} from '../../ui/sticker';
import {Slide} from '../../ui/slide';

interface IService {
  text: string;
  img: string;
}

const services: IService[] = [
  {text: 'Elektrische Instalationen', img: '/cards/electrical_inst.webp'},
  {text: 'Inteligentes Haussystem', img: '/cards/smart_home.webp'},
  {text: 'Alarmsysteme', img: '/cards/home_security.webp'},
  {text: 'Überwachungssysteme', img: '/cards/security.webp'},
  {text: 'Hauskino', img: '/cards/home_cinema.webp'},
  {text: 'Verkabelungungen', img: '/cards/wiring.webp'},
  {text: 'Erdungsanlagen, Blitzschutzanlagen', img: '/cards/lightning_safety.webp'},
  {text: 'Datennetz', img: '/cards/data_network.webp'},
  {text: 'Verkabelungswege bauen', img: '/cards/pathing.webp'}
];

const Component: React.FC = () => {
  return (
    <div className='page'>
      <Slide
        rows={3}
        title={{text:'Finden Sie die beste Lösung', position: 'bottom'}}
        link={{text: 'Finde uns', url: '/contact/'}}>
        <video className='content' autoPlay loop muted>
          <source src='/zwrEyYw.mp4' type='video/mp4' />
        </video>
      </Slide>
      <Slide
        rows={1}
        title={{text: 'Was bieten wir an?', position: 'top'}}
        link={{text: 'Prüfen Sie unsere Dienstleistungen:'}}>
      </Slide>
      <Slide rows={3}
        gap={20}>
        {
          services.map((service, index) => {
            return (<Sticker text={service.text} imgUrl={service.img} key={index} />);
          })
        }
      </Slide>
      <Slide
        rows={3}
        title={{text:'Gleiche Ziele, besser morgen', position: 'bottom'}}>
        <img className="content" src="/cards/better_tomorrow.webp" alt="better tomorrow" />
      </Slide>
      <Slide
        rows={1}
        title={{text:'Kontaktiere uns', position: 'top'}}
        link={{text: 'Gehen Sie zur Kontaktseite', url: '/contact/'}}>
      </Slide>
    </div>
  );
}

export const Home = Component;
